<?php
return [
    'service_manager' => [
        'factories' => [
            \TTF\V1\Rest\TTF\TTFResource::class => \TTF\V1\Rest\TTF\TTFResourceFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'ttf.rest.ttf' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/ttf[/:ttf_id]',
                    'defaults' => [
                        'controller' => 'TTF\\V1\\Rest\\TTF\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'ttf.rest.ttf',
        ],
    ],
    'zf-rest' => [
        'TTF\\V1\\Rest\\TTF\\Controller' => [
            'listener' => \TTF\V1\Rest\TTF\TTFResource::class,
            'route_name' => 'ttf.rest.ttf',
            'route_identifier_name' => 'ttf_id',
            'collection_name' => 'ttf',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \TTF\V1\Rest\TTF\TTFEntity::class,
            'collection_class' => \TTF\V1\Rest\TTF\TTFCollection::class,
            'service_name' => 'TTF',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'TTF\\V1\\Rest\\TTF\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'TTF\\V1\\Rest\\TTF\\Controller' => [
                0 => 'application/vnd.ttf.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'TTF\\V1\\Rest\\TTF\\Controller' => [
                0 => 'application/vnd.ttf.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \TTF\V1\Rest\TTF\TTFEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'ttf.rest.ttf',
                'route_identifier_name' => 'ttf_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \TTF\V1\Rest\TTF\TTFCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'ttf.rest.ttf',
                'route_identifier_name' => 'ttf_id',
                'is_collection' => true,
            ],
        ],
    ],
];
