<?php
namespace TTF\V1\Rest\TTF;

class TTFResourceFactory
{
    public function __invoke($services)
    {
        return new TTFResource();
    }
}
